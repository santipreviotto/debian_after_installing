# Things to Do After Installing Debian

## Add missing firmware

```
apt install firmware-atheros firmware-realtek intel-microcode
```

## Move to testing

Open repos list:

```
vim /etc/apt/sources.list
```

Comment all lines and add these:

```
deb http://deb.debian.org/debian/ bookworm main contrib non-free
deb-src http://deb.debian.org/debian/ bookworm main contrib non-free
```

Then run:

```
apt update && apt full-upgrade
```

## Debloat packages

```
apt autoremove --purge gnome-software gnome-clocks gnome-weather gnome-maps gnome-contacts gnome-2048 aisleriot gnome-chess totem five-or-more four-in-a-row hitori gnome-klotski lightsoff gnome-mahjongg gnome-mines gnome-nibbles quadrapassel iagno gnome-robots gnome-sudoku swell-foop synaptic tali gnome-taquin gnome-tetravex rhythmbox shotwell gnome-music gnome-tweaks evolution evolution-data-server
```

## Install basic utils packages 

```
apt install vim tmux git docker.io
```

Then create a ``.vimrc`` file with these settings:

```
"Set compatibility to vim only
set nocompatible

"Show lines numbers
set number

"Show status bar
set laststatus=2

"Insert space characters whenever the tab
set expandtab
set tabstop=4
retab

"Enable colors
syntax on
```
